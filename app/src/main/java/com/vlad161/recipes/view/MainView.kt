package com.vlad161.recipes.view

import com.vlad161.recipes.model.dto.RecipeDO

interface MainView {
    fun startLoad()
    fun showData(recipes: List<RecipeDO>)
    fun showError(text: String)
}