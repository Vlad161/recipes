package com.vlad161.recipes.view

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.text.TextUtils
import android.view.View
import android.widget.ProgressBar
import com.vlad161.recipes.R
import com.vlad161.recipes.model.dto.RecipeDO
import com.vlad161.recipes.other.di.DaggerViewComponent
import com.vlad161.recipes.other.di.ViewModule
import com.vlad161.recipes.presenter.MainPresenter
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter

    private val progressBar by lazy { findViewById(R.id.progress_bar) as ProgressBar }

    private val searchView by lazy { findViewById(R.id.search_view) as SearchView }

    private val recyclerView by lazy { findViewById(R.id.recycler_view) as RecyclerView }

    private val recyclerViewAdapter = RecyclerViewAdapter(this)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DaggerViewComponent.builder().viewModule(ViewModule(this)).build().inject(this)

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = recyclerViewAdapter

        searchView.setOnQueryTextListener(onQueryTextListener)
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun startLoad() {
        progressBar.visibility = View.VISIBLE
    }

    override fun showData(recipes: List<RecipeDO>) {
        progressBar.visibility = View.INVISIBLE
        recyclerViewAdapter.updateData(recipes)
        recyclerViewAdapter.notifyDataSetChanged()
    }

    override fun showError(text: String) {
        progressBar.visibility = View.INVISIBLE
        Snackbar.make(findViewById(android.R.id.content), text, Snackbar.LENGTH_SHORT).show()
    }

    private val onQueryTextListener: SearchView.OnQueryTextListener = object : SearchView.OnQueryTextListener {
        override fun onQueryTextSubmit(query: String?): Boolean {
            return true
        }

        override fun onQueryTextChange(newText: String): Boolean {
            if (TextUtils.isEmpty(newText)) {
                showData(arrayListOf())
            } else {
                presenter.queryChanged(newText)
            }
            return true
        }
    }
}