package com.vlad161.recipes.view

import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.vlad161.recipes.model.dto.RecipeDO

class RecyclerViewAdapter(val context: Context) : RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {

    @LayoutRes
    private val resource = android.R.layout.simple_list_item_1

    private val layoutInflate by lazy { LayoutInflater.from(context) }

    private var recipesList = emptyList<RecipeDO>()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(layoutInflate.inflate(resource, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.tvText?.text = recipesList[position].title
    }

    override fun getItemCount() = recipesList.size

    fun updateData(list: List<RecipeDO>) {
        recipesList = list
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvText = itemView.findViewById(android.R.id.text1) as TextView
    }
}