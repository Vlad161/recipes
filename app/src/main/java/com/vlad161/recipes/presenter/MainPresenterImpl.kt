package com.vlad161.recipes.presenter

import com.vlad161.recipes.model.Model
import com.vlad161.recipes.other.MyApplication
import com.vlad161.recipes.view.MainView
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MainPresenterImpl(val mainView: MainView) : MainPresenter {

    @Inject
    lateinit var dataRepository: Model

    private val compositeDisposable = CompositeDisposable()


    init {
        MyApplication.appComponent.inject(this)
    }

    override fun onStop() {
        compositeDisposable.clear()
    }

    override fun queryChanged(query: String) {
        compositeDisposable.clear()
        mainView.startLoad()
        compositeDisposable.add(dataRepository.getRecipes(query).subscribe({
            mainView.showData(it)
        }, {
            mainView.showError(it.message.toString())
        }))
    }
}