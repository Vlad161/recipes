package com.vlad161.recipes.presenter

interface MainPresenter {
    fun onStop()
    fun queryChanged(query: String)
}