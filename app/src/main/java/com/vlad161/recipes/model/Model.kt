package com.vlad161.recipes.model

import com.vlad161.recipes.model.dto.RecipeDO
import io.reactivex.Single

interface Model {

    fun getRecipes(query: String): Single<List<RecipeDO>>
}