package com.vlad161.recipes.model.api

import com.vlad161.recipes.model.dto.SearchRecipesDO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {

    @GET("api/")
    fun getRecipes(@Query("q") query: String): Single<SearchRecipesDO>
}