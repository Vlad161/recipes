package com.vlad161.recipes.model.dto

data class SearchRecipesDO(val results: ArrayList<RecipeDO>)