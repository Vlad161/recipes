package com.vlad161.recipes.model

import com.vlad161.recipes.model.api.ApiInterface
import com.vlad161.recipes.model.dto.RecipeDO
import com.vlad161.recipes.other.IO_THREAD
import com.vlad161.recipes.other.MyApplication
import com.vlad161.recipes.other.UI_THREAD
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.SingleTransformer
import javax.inject.Inject
import javax.inject.Named

class ModelImpl : Model {

    @Inject
    lateinit var apiInterface: ApiInterface

    @field:[Inject Named(UI_THREAD)]
    lateinit var uiScheduler: Scheduler

    @field:[Inject Named(IO_THREAD)]
    lateinit var ioScheduler: Scheduler

    init {
        MyApplication.appComponent.inject(this)
    }

    override fun getRecipes(query: String): Single<List<RecipeDO>> {
        return apiInterface.getRecipes(query)
                .toObservable()
                .flatMapIterable { it.results }
                .take(20)
                .toList()
                .compose(applySchedulers())
    }

    private fun <T> applySchedulers(): SingleTransformer<T, T> = SingleTransformer {
        it.subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
    }
}