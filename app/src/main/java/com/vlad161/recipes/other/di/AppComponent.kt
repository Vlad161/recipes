package com.vlad161.recipes.other.di

import com.vlad161.recipes.model.ModelImpl
import com.vlad161.recipes.presenter.MainPresenterImpl
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ModelModule::class, PresenterModule::class))
interface AppComponent {

    fun inject(modelImpl: ModelImpl)

    fun inject(mainPresenterImpl: MainPresenterImpl)
}