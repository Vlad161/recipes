package com.vlad161.recipes.other

import android.app.Application
import com.vlad161.recipes.other.di.AppComponent
import com.vlad161.recipes.other.di.DaggerAppComponent

open class MyApplication : Application() {

    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        buildComponent()
    }

    protected open fun buildComponent() {
        appComponent = DaggerAppComponent.create()
    }
}