package com.vlad161.recipes.other.di

import com.vlad161.recipes.view.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ViewModule::class))
interface ViewComponent {

    fun inject(mainActivity: MainActivity)
}