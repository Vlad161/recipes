package com.vlad161.recipes.other.di

import com.vlad161.recipes.model.Model
import com.vlad161.recipes.model.ModelImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class PresenterModule {

    @Provides
    @Singleton
    fun provideDataRepository(): Model {
        return ModelImpl()
    }
}