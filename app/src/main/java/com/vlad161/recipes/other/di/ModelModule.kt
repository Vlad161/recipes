package com.vlad161.recipes.other.di

import com.vlad161.recipes.model.api.ApiInterface
import com.vlad161.recipes.model.api.ApiModule
import com.vlad161.recipes.other.BASE_URL
import com.vlad161.recipes.other.IO_THREAD
import com.vlad161.recipes.other.UI_THREAD
import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Named
import javax.inject.Singleton

@Module
class ModelModule {

    @Provides
    @Singleton
    fun provideApiInterface(): ApiInterface {
        return ApiModule.getApiInterface(BASE_URL)
    }

    @Provides
    @Singleton
    @Named(UI_THREAD)
    fun provideSchedulerUI() = AndroidSchedulers.mainThread()

    @Provides
    @Singleton
    @Named(IO_THREAD)
    fun provideSchedulerIO() = Schedulers.io()
}