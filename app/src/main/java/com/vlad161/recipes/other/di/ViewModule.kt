package com.vlad161.recipes.other.di

import com.vlad161.recipes.presenter.MainPresenter
import com.vlad161.recipes.presenter.MainPresenterImpl
import com.vlad161.recipes.view.MainView
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModule(val mainView: MainView) {

    @Provides
    @Singleton
    fun provideMainPresenter(): MainPresenter {
        return MainPresenterImpl(mainView)
    }
}