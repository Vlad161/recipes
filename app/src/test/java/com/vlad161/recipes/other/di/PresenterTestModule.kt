package com.vlad161.recipes.other.di

import com.vlad161.recipes.model.Model
import com.vlad161.recipes.model.ModelImpl
import dagger.Module
import dagger.Provides
import org.mockito.Mockito.mock
import javax.inject.Singleton

@Module
class PresenterTestModule {

    @Provides
    @Singleton
    fun provideDataRepository(): Model {
        return mock(ModelImpl::class.java)
    }
}