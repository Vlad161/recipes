package com.vlad161.recipes.other.di

import com.vlad161.recipes.model.api.ApiInterface
import com.vlad161.recipes.other.IO_THREAD
import com.vlad161.recipes.other.UI_THREAD
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import org.mockito.Mockito.mock
import javax.inject.Named
import javax.inject.Singleton

@Module
class ModelTestModule {

    @Provides
    @Singleton
    fun provideApiInterface(): ApiInterface {
        return mock(ApiInterface::class.java)
    }

    @Provides
    @Singleton
    @Named(UI_THREAD)
    fun provideSchedulerUI() = Schedulers.trampoline()

    @Provides
    @Singleton
    @Named(IO_THREAD)
    fun provideSchedulerIO() = Schedulers.trampoline()
}