package com.vlad161.recipes.other

import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder

class TestUtils {

    private val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    fun <T> readJson(fileName: String, inClass: Class<T>): T {
        return gson.fromJson(readString(fileName), inClass)
    }

    private fun readString(fileName: String): String? {
        return this.javaClass.getResource(fileName).readText()
    }
}