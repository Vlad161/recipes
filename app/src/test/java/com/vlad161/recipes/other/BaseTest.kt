package com.vlad161.recipes.other

import com.vlad161.recipes.BuildConfig
import com.vlad161.recipes.other.di.TestComponent
import org.junit.Before
import org.junit.Ignore
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(application = TestApplication::class,
        constants = BuildConfig::class,
        sdk = intArrayOf(21))
@Ignore
open class BaseTest {

    val testUtils = TestUtils()

    lateinit var testComponent: TestComponent

    @Before
    open fun setUp() {
        testComponent = TestApplication.testComponent
    }
}