package com.vlad161.recipes.other

import com.vlad161.recipes.other.di.AppComponent
import com.vlad161.recipes.other.di.DaggerTestComponent
import com.vlad161.recipes.other.di.TestComponent

class TestApplication : MyApplication() {

    companion object {
        lateinit var testComponent: TestComponent
    }

    override fun onCreate() {
        super.onCreate()
        testComponent = appComponent as TestComponent
    }

    override fun buildComponent() {
        appComponent = DaggerTestComponent.create() as AppComponent
    }
}