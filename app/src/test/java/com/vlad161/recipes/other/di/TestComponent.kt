package com.vlad161.recipes.other.di

import com.vlad161.recipes.model.ModelImplTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(ModelTestModule::class, PresenterTestModule::class))
interface TestComponent : AppComponent {
    fun inject(modelImplTest: ModelImplTest)
}