package com.vlad161.recipes.model

import com.vlad161.recipes.model.api.ApiInterface
import com.vlad161.recipes.model.dto.SearchRecipesDO
import com.vlad161.recipes.other.BaseTest
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import javax.inject.Inject

class ModelImplTest : BaseTest() {

    @Inject
    lateinit var api: ApiInterface

    private val model = ModelImpl()

    @Before
    override fun setUp() {
        super.setUp()
        testComponent.inject(this)
    }

    @Test
    fun testSearchRecipes() {
        val searchRecipes = testUtils.readJson("/json/query_omelet_30_size.json", SearchRecipesDO::class.java)
        `when`(api.getRecipes("omelet")).thenReturn(Single.just(searchRecipes))

        val testObserver = model.getRecipes("omelet").test()

        testObserver.assertNoErrors()
        testObserver.assertValueCount(1)

        val recipes = testObserver.values()[0]

        assertEquals(20, recipes.size)
    }
}